app.controller("demoCtrl", function($scope,$http,$interval,$filter,$timeout) {

	$scope.timeout = 1000;
	$scope.current_value = "";
	$scope.trip_state = false;
	$scope.inventer_state = false;
    $scope.graph_data  = [];
    $scope.download = function() {
        $scope.message = "";
    };
    $time = $filter('date')(new Date(), 'h:m:s');
    $scope.labels = [];
    $scope.series = ["Frequency"];
    $scope.data = {
		      labels: $scope.labels,
		      datasets: [
		        {
		          label: 'Actual',
		          fillColor: 'rgba(151,187,205,0.2)',
		          strokeColor: 'rgba(151,187,205,1)',
		          pointColor: 'rgba(151,187,205,1)',
		          pointStrokeColor: '#fff',
		          pointHighlightFill: '#fff',
		          pointHighlightStroke: 'rgba(151,187,205,1)',
		          data: $scope.graph_data
		        }]};
    
    $scope.read  = function() {
    	if($scope.is_request_progress){
    		return;
    	}
    	
    	$http.get("php/read.php")
        .success(function(response) {
        	if(response.frequency == ""){
        		response.frequency = $scope.lowerLimit;
        	}
        	 var value = parseInt(response.frequency);
        	 if(value < $scope.lowerLimit){
        		 $scope.value =  $scope.lowerLimit;
        	 }else if(value > $scope.upperLimit){
        		 $scope.value =  $scope.upperLimit;
        	 }else{
        		 $scope.value = value;
        	 }
        	 $scope.current_value = response.frequency;
        	 if(!$scope.is_request_progress){
        		 $scope.trip_state = response.trip_state;
        		 $scope.inventer_state = response.inventer_state;
        		 $scope.old_trip_state = response.trip_state;
        		 $scope.old_inventer_state = response.inventer_state;
        	 }
        	 $scope.trip_active = response.trip_active;
        	 $time = $filter('date')(new Date(), 'h:m:s');
         	 //$scope.labels.push($time);
         	 if($scope.graph_data.length == 8){
         		 var ss = angular.copy($scope.data.datasets[0].data);
         		ss.splice(0,1);
         		ss.push(response.frequency);
         		$scope.data.datasets[0].data = angular.copy(ss);
         		$scope.labels.splice(0,1);
         		$scope.labels.push($time)
         		
         	 }else{
         		 $scope.graph_data.push(response.voltage_value);
         		 $scope.labels.push($time)
         	 }
        });
    };
    
    
    $interval($scope.read, $scope.timeout);
    
    $scope.value = 800;
    $scope.upperLimit = 1200;
    $scope.lowerLimit = 800;
    $scope.unit = "Hz";
    $scope.precision = 1;
    $scope.ranges = [
        {
            min: 800,
            max: 900,
            color: '#DEDEDE'
        },
        {
            min: 900,
            max: 1000,
            color: '#8DCA2F'
        },
        {
            min: 1000,
            max: 1100,
            color: '#FDC702'
        },
        {
            min: 1100,
            max: 1150,
            color: '#FF7700'
        },
        {
            min: 1150,
            max: 1200,
            color: '#C50200'
        }
    ];

    $scope.labels1 = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
    $scope.data1= [
      [0,0,0,0,0,0,0,0]
    
    ];
    $scope.colours1 = [
      { // grey
        fillColor: 'rgba(148,159,177,0.2)',
        strokeColor: 'rgba(148,159,177,1)',
        pointColor: 'rgba(148,159,177,1)',
        pointStrokeColor: '#fff',
        pointHighlightFill: '#fff',
        pointHighlightStroke: 'rgba(148,159,177,0.8)'
      },
      { // dark grey
        fillColor: 'rgba(77,83,96,0.2)',
        strokeColor: 'rgba(77,83,96,1)',
        pointColor: 'rgba(77,83,96,1)',
        pointStrokeColor: '#fff',
        pointHighlightFill: '#fff',
        pointHighlightStroke: 'rgba(77,83,96,1)'
      }
    ];
    $scope.randomize = function () {
      $scope.data1 = $scope.data1.map(function (data) {
        return data.map(function (y) {
          y = y + Math.random() * 10 - 5;
          return parseInt(y < 0 ? 0 : y > 100 ? 100 : y);
        });
      });
    };
    
    //$("[name='my-checkbox']").bootstrapSwitch();
    
    
    $scope.isSelected = 'false';
    $scope.isActive = true;
    $scope.size = 'normal';
    $scope.animate = true;
   
    $scope.change_state = function(button,value){
    	if((button == 'trip' && value == $scope.old_trip_state) || (button == 'inventer' && value == $scope.old_inventer_state)){
    		return;
    	}
    	
    	$scope.is_request_progress = true;
    	data = {
    			'key' : button,
    			'state' : value
    	};
    	$http.post("php/write.php",data)
        .success(function(response) {
         	$scope.is_request_progress = false;
         	$scope.read();
        });
    }
  
   
 });
