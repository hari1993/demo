#include"gpio.c" 
#include"gpio.h"
#include <sys/time.h>

int main(int argc, char *argv[])
{
	// Declaring variable
	unsigned int PIN;
	struct timeval start, end;
	long mtime, seconds, useconds;
	int count = 0 , flag = 1;
	unsigned int sample_rate = 200;

    // Checking and initial MMAP
    if (argc < 2)
    {
    	perror("PIN MISSING");
    	exit(1);
    }

    PIN = atoi(argv[1]);

    if(argc > 1){
    	sample_rate = atoi(argv[2]);
    }

	// Checking and initial MMAP
	if (gpioInit()!=0)
	{
		perror("GPIO Initialization Error");
		exit(1);
	}

	// Set PIN direction as input
	if(gpioSetPinDirection(PIN,INPUT)!=0)
	{
		perror("Error setting the PIN Direction");
		exit(1);
	}

	// Wait for postive attitude
	while(! gpioReadPin(PIN)){

	}

	// Get start time
	gettimeofday(&start, NULL);

	while(count < sample_rate)
	{
		if(gpioReadPin(PIN)){
			flag = 1;
		}else if(flag == 1){
			count++;
			flag = 0;	
		}
	}

	// Get end time
	gettimeofday(&end, NULL);

	// Calculate time in Micro second
	seconds  = end.tv_sec  - start.tv_sec;
	useconds = end.tv_usec - start.tv_usec;
	mtime = seconds*1000000 + useconds;
	cleanup();
	return (1000000/(mtime/sample_rate));
}

