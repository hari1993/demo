<?php

define('DS', DIRECTORY_SEPARATOR);
define('BASE_PATH', dirname(__FILE__).'/..');
define('CONTEXT_PATH', BASE_PATH . DS);
define('PHP_CODE_PATH', CONTEXT_PATH . 'php' . DS);
define('CONFIG_PATH', CONTEXT_PATH . 'config' . DS);
header('Access-Control-Allow-Origin: *');

$error_log = "/var/portal/log/demo_log_".date("Y-m-d").".log";
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1);
ini_set("log_errors" , "1");
ini_set("error_log" , $error_log);

require_once CONFIG_PATH . 'config.php';

require_once  PHP_CODE_PATH. 'utilitymethods.php';

require_once  PHP_CODE_PATH. 'beaglehelper.php';

