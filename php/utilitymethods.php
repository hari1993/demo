<?php

function get_gpio_value_path($pin){
	return GPIO_BASE_PATH."/gpio".$pin."/value";
}

function get_gpio_ditection_path($pin){
	return GPIO_BASE_PATH."/gpio".$pin."/direction";
}

function get_gpio_base_path($pin){
	return GPIO_BASE_PATH."/gpio".$pin;
}

function get_gpio_export_path(){
	return GPIO_BASE_PATH."/export";
}

function get_analog_path($pin){
	return ANALOG_BASE_PATH.$pin;
}

