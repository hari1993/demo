<?php 

require_once 'initiator.php';

$response = array();
if(!file_exists(FREQUENCY_TEMP_VALUE)){
	error_log(" Unable to read voltage value : program missing");
	$response['frequency'] = 0;
}
$response['trip_active'] = true;
$value = file_get_contents(FREQUENCY_TEMP_VALUE); 
if($value === false){
	error_log(" Unable to read voltage value");
	$response['frequency'] = 0;
}else{
	$response['frequency'] = $value;
	if($value > TRIP_OFF_VOLTAGE){
		$response['trip_active'] = false;
	}
}

$trip_value = BeagleHelper::get_gpio_value(TRIP_GPIO_PIN);

if($trip_value === false){
	error_log(" Unable to read trip switch value");
	$response['trip_state'] = false;
}else{
	$response['trip_state'] = ($trip_value == 1)? true : false;
}

$inventer_value = BeagleHelper::get_gpio_value(INVENTER_GPIO_PIN);

if($inventer_value === false){
	error_log(" Unable to read trip switch value");
	$response['inventer_state'] = false;
}else{
	$response['inventer_state'] = ($inventer_value == 1)? true : false;
}


echo json_encode($response);
