<?php

class BeagleHelper {

	public static function init_gpio($pin, $purpose = "out", $sudo = false){
		if(is_dir(GPIO_BASE_PATH)){
			shell_exec("echo ".$pin." > ".get_gpio_export_path());
			if(!is_dir(get_gpio_base_path($pin))){
				error_log("Some thing went wrong!. Unable to create GPIO for GPIO PIN : ".$pin." and Purpose : ".$purpose);
				return false;
			}
			shell_exec("echo ".$purpose." > ".get_gpio_ditection_path($pin));
			if($sudo){
				shell_exec("chmod 777 ".get_gpio_value_path($pin));
			}
			return get_gpio_value_path($pin);
		}else{
			error_log("GPIO Path not exist!");
			return false;
		}
		
	}  
	
	public static function set_gpio_value($pin,$value){
		if(!file_exists(get_gpio_value_path($pin))){
			if(self::init_gpio($pin) === false){
				error_log("Unable to set value for GPIO, PIN : ".$pin);
				return false;
			}
		}
		file_put_contents(get_gpio_value_path($pin), $value);
		
	}
	
	public static function get_gpio_value($pin){
		if(!file_exists(get_gpio_value_path($pin))){
			if(self::init_gpio($pin) === false){
				error_log("Unable to get value for GPIO, PIN : ".$pin);
				return false;
			}
		}
		return file_get_contents(get_gpio_value_path($pin));
	}
	
	public static function get_analog_value($pin){
		if(!file_exists(get_analog_path($pin))){
			error_log("Unable to get analog value for PIN : ".$pin);
			return false;
		}
		return file_get_contents(get_analog_path($pin));
	}
	
	
} 