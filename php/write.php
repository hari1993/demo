<?php 

require_once 'initiator.php';
$input_content = file_get_contents("php://input");
$input_array = json_decode($input_content,true);

$key = $input_array['key'];
$state = ($input_array['state'] == true)? 1 : 0;

if($key == 'trip'){
	if(file_exists(FREQUENCY_TEMP_VALUE)){
		$value = file_get_contents(FREQUENCY_TEMP_VALUE);
		
		if($value !== false){
			if($value > TRIP_OFF_VOLTAGE){
				return 0;
			}
		}
	}
	
	BeagleHelper::set_gpio_value(TRIP_GPIO_PIN,$state);
	if($state){
		BeagleHelper::set_gpio_value(INVENTER_GPIO_PIN,0);
	}
	
}else{
	$trip_value = BeagleHelper::get_gpio_value(TRIP_GPIO_PIN);
	if($trip_value == 1){
		return 0;
	}
	BeagleHelper::set_gpio_value(INVENTER_GPIO_PIN,$state);
}

