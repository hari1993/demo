<?php

require_once '/var/www/html/php/initiator.php';

ini_set("error_log" , "/var/portal/log/monitor_log_".date("Y-m-d").".log");

$max_value = TRIP_OFF_VOLTAGE;
$old_value = 0;

BeagleHelper::init_gpio(TRIP_GPIO_PIN,'out',true);
BeagleHelper::init_gpio(INVENTER_GPIO_PIN,'out',true);

BeagleHelper::init_gpio(67,'out',true);
BeagleHelper::init_gpio(68,'out',true);
BeagleHelper::init_gpio(44,'out',true);
BeagleHelper::init_gpio(26,'out',true);


while(1){
	if(!file_exists(FREQUENCY_READ_PROGRAM)){
		error_log(" Unable to read frequency : program missing");
		exit;
	}
	$output = array();
	$return_value = NULL;
	
	exec(FREQUENCY_READ_PROGRAM." ".FREQUENCY_INPUT_PIN,$output,$return_value);
	if($return_value == 0){
		sleep(1);
		continue;
	}
	
	$frequency = isset($output[0])? $output[0] : 0;
	if($old_value != $frequency){
		if($frequency > $max_value){
			BeagleHelper::set_gpio_value(TRIP_GPIO_PIN, 0);
			BeagleHelper::set_gpio_value(67, 1);
		}else{
			BeagleHelper::set_gpio_value(67, 0);
		}
		if($frequency > 1000){
			BeagleHelper::set_gpio_value(68, 1);
		}else{
			BeagleHelper::set_gpio_value(68, 0);
		}
		if($frequency > 950){
			BeagleHelper::set_gpio_value(44, 1);
		}else{
			BeagleHelper::set_gpio_value(44, 0);
		}
		if($frequency > 900){
			BeagleHelper::set_gpio_value(26, 1);
		}else{
			BeagleHelper::set_gpio_value(26, 0);
		}
		$old_value = $frequency;
		file_put_contents(FREQUENCY_TEMP_VALUE, $frequency);
		error_log(" Frequency : ".$frequency);
	}
	usleep(50000);
}
