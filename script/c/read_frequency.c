#include"gpio.c" 
#include"gpio.h"
#include <sys/time.h>

int main(int argc, char *argv[])
{
	// Declaring variable
	unsigned int PIN;
	struct timeval start, end, program_start, current;
	long mtime, seconds, useconds;
	int count = 0 , flag = 1;
	unsigned int sample_rate = 500;

    // Checking and initial MMAP
    if (argc < 2)
    {
    	perror("PIN MISSING");
    	exit(0);
    }

    PIN = atoi(argv[1]);

    if(argc > 2){
    	sample_rate = atoi(argv[2]);
    }

	// Checking and initial MMAP
	if (gpioInit()!=0)
	{
		perror("GPIO Initialization Error");
		exit(0);
	}

	// Set PIN direction as input
	if(gpioSetPinDirection(PIN,INPUT)!=0)
	{
		perror("Error setting the PIN Direction");
		exit(0);
	}
	gettimeofday(&program_start, NULL);
	// Wait for postive attitude
	while(! gpioReadPin(PIN)){
		gettimeofday(&current, NULL);
		if(current.tv_sec > (program_start.tv_sec + 2) ){
			exit(0);
		}
	}

	// Get start time
	gettimeofday(&start, NULL);

	while(count < sample_rate)
	{
		if(gpioReadPin(PIN)){
			flag = 1;
		}else if(flag == 1){
			count++;
			flag = 0;	
		}
		gettimeofday(&current, NULL);
		if(current.tv_sec > (program_start.tv_sec + 2) ){
			exit(0);
		}
	}

	// Get end time
	gettimeofday(&end, NULL);

	// Calculate time in Micro second
	seconds  = end.tv_sec  - start.tv_sec;
	useconds = end.tv_usec - start.tv_usec;
	mtime = seconds*1000000 + useconds;
	cleanup();
	printf("%ld", (1000000/(mtime/sample_rate)));
	return 1;
}

