/**********************************************************

This is a test program that tests the I/O library.
When the Receiver detects that the LASER has been intercepted,
it changes the direction (orientation) of the Stepper Motor 

***********************************************************/


#include"gpio.c" 
#include"gpio.h"
			

int main(int argc, char *argv[])
{
	int i=0,t=1;   
 	unsigned int DIR = 60;
	int time = strtol(argv[1], NULL, 10) * 1000;
	if (gpioInit()!=0)
    {
        perror("GPIO Initialization Error");
        exit(1);
    }


    if(gpioSetPinDirection(DIR,OUTPUT)!=0)
    {
        perror("Error setting the PIN Direction");
        exit(1);
    }
    while(1)
	{
		gpioWritePin( DIR, LOW );
		usleep(time);
printf("%d",gpioReadPin(DIR));
if(!gpioReadPin(DIR)){
	printf("OFF\n");
}

		gpioWritePin( DIR, HIGH );
		usleep(time);
if(gpioReadPin(DIR)){ 
        printf("ON\n");
}

printf("%d",gpioReadPin(DIR));
i++;
	}
//	cleanup();
    return 0;
}

