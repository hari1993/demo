<?php

define('SERVER_ADDRESS','127.0.0.1');
define("GPIO_BASE_PATH", "/sys/class/gpio");
define('ANALOG_BASE_PATH','/sys/devices/ocp.3/helper.15/AIN');
define('FREQUENCY_READ_PROGRAM', "/var/portal/script/c/read_frequency");
define('FREQUENCY_TEMP_VALUE', "/var/www/html/php/frequency.dat");
define('TRIP_GPIO_PIN','66');
define('INVENTER_GPIO_PIN', '69');
define('FREQUENCY_INPUT_PIN','60');

define('TRIP_OFF_VOLTAGE',1050);
